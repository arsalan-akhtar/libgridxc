#!/bin/sh

# Helper script to install the full range of flavors of libgridxc (assuming libxc functionality).
# It uses the '--enable-multiconfig' feature to tag the different flavors appropriately in the
# installation directory.

# These variables should be defined, either through environmental variables or as script arguments.

#GRIDXC_PREFIX=/tmp/GXC_MULTI

#FC=gfortran
#MPIFC=pmpif90
#MPICC=mpicc

#LIBXC_ROOT  # Installation location of libxc.
#MPI_ROOT    # Installation location of the mpi framework.

#
#
rm -rf build_dp_mpi; mkdir build_dp_mpi; cd build_dp_mpi
FC=$MPIFC CC=$MPICC ../configure --enable-multiconfig  --with-mpi=${MPI_ROOT} --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_PREFIX}
make
make check
make install
cd ..
#
rm -rf build_sp_mpi; mkdir build_sp_mpi; cd build_sp_mpi
FC=$MPIFC CC=$MPICC ../configure --enable-multiconfig  --with-mpi=${MPI_ROOT} --enable-single-precision --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_PREFIX}
make
make check
make install
cd ..
#
rm -rf build_dp; mkdir build_dp; cd build_dp
../configure --enable-multiconfig --without-mpi --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_PREFIX}
make
make check
make install
cd ..
#
rm -rf build_sp ; mkdir build_sp; cd build_sp
../configure --enable-multiconfig --without-mpi --enable-single-precision --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_PREFIX}
make
make check
make install
cd ..


