---
project: libGridXC
summary: Library to compute exchange-correlation energies and potentials
author: Jose Soler, Alberto Garcia
project_dir: src
page_dir: docs/pages
output_dir: /tmp/gridxc-docs
exclude: alloc.F90
         am05.F90
         array.F90
         bessph.F90
	 build.sh
         cellsubs.F90
         chkgmx.F90
	 config.sh
         debugxc.F90
         fft3d.F90
         fftr.F90
         ggaxc.F90
         gpfa_core.F
         interpolation.f90
         ldaxc.F90
         m_fft_gpfa.F
         m_io.F90
         m_walltime.f90
         mesh1d.f90
         mesh3d.F90
         minvec.F90
         moreParallelSubs.F90
         precision.F90
         radfft.F90
         setgpfa.f
         sorting.F90
         sys.F90
         vdwxc.F90
         vv_vdwxc.F90
preprocessor: gfortran -E -P
preprocess: true
macro: HAVE_LIBXC
       HAVE_MPI
project_website: http://gitlab.com/siesta-project/libraries/libgridxc
email: albertog@icmab.es
extensions: f90
            F90
docmark: +	    
docmark_alt: *
predocmark: >
predocmark_alt: <
media_dir: ./media
source: false
graph: false
search: false
license: bsd
display: public
	 protected
extra_filetypes: sh #
md_extensions: markdown.extensions.toc
---

The library provides an API for computing the XC energies and
potentials for radial and box grids.

For more information, check out the [Overview](./page/index.html).
