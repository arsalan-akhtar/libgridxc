#!/bin/bash
#
# Copyright (C) 2015 Y. Pouillon
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 3 of the License, or 
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
#

# Note: this script is temporary and will be removed upon release.

# Set # of procs for make
test -n "${make_nprocs}" || make_nprocs="8"

# Init modules if required
export LMOD_PAGER="none"
type module >/dev/null 2>&1
if test "$?" != "0"; then
  for pfx in /usr /usr/local /opt /opt/local; do
    lmod_init="${pfx}/share/lmod/lmod/init/bash"
    if test -s "${lmod_init}"; then
      . "${lmod_init}"
      break
    fi
  done
fi

# Load strictly required modules
module purge
module load foss/2016b
module load libxc/2.2.3-foss-2016b
module list
sleep 1

# Stop at first error and echo commands
set -ev

# Check that we are in the correct directory
test -s "configure.ac" -a -s "src/atomxc.F90" || exit 0

# Fix permissions
chmod -R u+w .

# Init build parameters
export CC="gcc"
export CFLAGS="-O0 -g3 -ggdb -Wall -Wextra -fbounds-check -fno-inline"
export FC="gfortran"
export FCFLAGS="-O0 -g3 -ggdb -Wall -Wextra -fbounds-check -fno-inline"

# Prepare source tree
./wipeout.sh
./autogen.sh

# Check minimal build
mkdir tmp-minimal
cd tmp-minimal
../configure \
  --disable-debug \
  --disable-multiconfig \
  --disable-single-precision \
  --without-libxc \
  --without-mpi
sleep 3
make dist -j${make_nprocs}
make -j${make_nprocs}
make clean && make -j${make_nprocs}
make check -j${make_nprocs}
mkdir install-test
make install DESTDIR="${PWD}/install-test"
ls -lR install-test >install-test.log
libname_ok=`find install-test/usr/local/lib -name 'libgridxc.*'`
test "${libname_ok}" != ""
modname_ok=`find install-test/usr/local/include -name 'gridxc'`
test "${modname_ok}" != ""
cd ..

# Build with LibXC support
mkdir tmp-libxc
cd tmp-libxc
../configure \
  --disable-debug \
  --with-libxc \
  --without-mpi
sleep 3
make -j${make_nprocs}
make check -j${make_nprocs}
mkdir install-test
make install DESTDIR="${PWD}/install-test"
libname_ok=`find install-test/usr/local/lib -name 'libgridxc.*'`
modname_ok=`find install-test/usr/local/include -name 'gridxc'`
test "${libname_ok}" != ""
cd ..

# Build with debugging features
mkdir tmp-debug
cd tmp-debug
../configure \
  --enable-debug \
  --disable-single-precision \
  --with-libxc \
  --without-mpi
sleep 3
make -j${make_nprocs}
make check -j${make_nprocs}
mkdir install-test
make install DESTDIR="${PWD}/install-test"
libname_ok=`find install-test/usr/local/lib -name 'libgridxc.*'`
test "${libname_ok}" != ""
cd ..

# Build with single precision
# FIXME: fails to build because of interface mismatch in fft3d.F90
#mkdir tmp-single
#cd tmp-single
#../configure \
#  --enable-debug \
#  --enable-single-precision \
#  --with-libxc \
#  --without-mpi
#sleep 3
#make -j${make_nprocs}
#make check -j${make_nprocs}
#mkdir install-test
#make install DESTDIR="${PWD}/install-test"
#libname_ok=`find install-test/usr/local/lib -name 'libgridxc.*'`
#test "${libname_ok}" != ""
#cd ..

# Build with MPI support
export CC="mpicc"
export FC="mpifort"
export F77="mpifort"
mkdir tmp-mpi
cd tmp-mpi
../configure \
  --disable-debug \
  --with-libxc \
  --with-mpi
sleep 3
make -j${make_nprocs}
make check -j${make_nprocs}
mkdir install-test
make install DESTDIR="${PWD}/install-test"
libname_ok=`find install-test/usr/local/lib -name 'libgridxc.*'`
test "${libname_ok}" != ""
cd ..

# Check the use of --enable-multiconfig
mkdir tmp-multiconfig
cd tmp-multiconfig
../configure \
  --disable-debug \
  --enable-multiconfig \
  --with-libxc \
  --with-mpi
sleep 3
make -j${make_nprocs}
make check -j${make_nprocs}
mkdir install-test
make install DESTDIR="${PWD}/install-test"
libname_ok=`find install-test/usr/local/lib -name 'libgridxc_dp_mpi.*'`
modname_ok=`find install-test/usr/local/include -name 'gridxc_dp_mpi'`
test "${libname_ok}" != ""
test "${modname_ok}" != ""
cd ..

# Make distcheck
mkdir tmp-distcheck
cd tmp-distcheck
../configure
sleep 3
make distcheck -j${make_nprocs}
make distcleancheck
cd ..

# Clean-up the mess
rm -rf tmp-minimal tmp-libxc tmp-single tmp-debug tmp-mpi tmp-multiconfig tmp-distcheck
