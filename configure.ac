# -*- Autoconf -*-
#
# Configure script for the LibGridXC package
#
# Copyright (C) 2017 Y. Pouillon
#
# This file is part of the LibGridXC software package. For license information,
# please see the COPYING file in the top-level directory of the source
# distribution.
#

                    # ------------------------------------ #

#
# Autotools startup
#

# Init Autoconf
AC_PREREQ(2.69)
AC_INIT([LibGridXC], [0.9.4.2],
  [https://gitlab.com/siesta-project/libgridxc/issues],
  [libgridxc],
  [https://gitlab.com/siesta-project/libgridxc])
AC_REVISION([Autotools support for LibGridXC])
AC_CONFIG_AUX_DIR(config/gnu)
AC_CONFIG_MACRO_DIR([config/m4])
AC_CONFIG_SRCDIR(src/atomxc.F90)
_AC_SRCDIRS([.])

# Init Automake
AC_CANONICAL_TARGET
AM_INIT_AUTOMAKE([1.14 parallel-tests])
AM_CONFIG_HEADER([config.h])

# Generate timestamp
gxc_timestamp=`date '+%Y%m%dT%H%M%S%z'`
AC_SUBST(gxc_timestamp)

                    # ------------------------------------ #

#
# System utilities
#

# Check for common programs
AC_PROG_MAKE_SET
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_SED
AC_PROG_AWK
AC_PROG_GREP

                    # ------------------------------------ #

#
# Default settings
#

# Note for developers: you may edit these parameters to tune the behaviour
# of the build system. Please do it with care.

# Lists of options to manage (gxc_*_enable_def variables)
#
# Note:
#   * switches (--enable-*) must be set to "yes" or "no"
#   * external dependencies (--with-*) must be set to "yes", "no", or "auto"
#
gxc_cfg_switches="debug multiconfig old_cray single"
gxc_cfg_extdeps="libxc mpi"

# Optional features
gxc_debug_enable_def="no"
gxc_multiconfig_enable_def="no"
gxc_old_cray_enable_def="no"
gxc_single_enable_def="no"

# LibXC
gxc_libxc_enable_def="auto"
gxc_libxc_incs_def=""
gxc_libxc_libs_def="-lxcf90 -lxc"

# MPI
gxc_mpi_enable_def="auto"
gxc_mpi_incs_def=""
gxc_mpi_libs_def="-lmpi"

# Internal variables
ARCH_SUFFIX=""
PREC_SUFFIX=""
gxc_precision="double"

                    # ------------------------------------ #

#
# Check consistency of defaults
#

# Default triggers for optional features must be yes or no, and not empty
for gxc_cfg_default in ${gxc_cfg_switches}; do
  tmp_default=`eval echo \$\{gxc_${gxc_cfg_default}_enable_def\}`
  if test "${tmp_default}" != "no" -a \
          "${tmp_default}" != "yes"; then
    AC_MSG_ERROR([invalid default value: gxc_${gxc_cfg_default}_enable_def = ${tmp_default}])
  fi
done

# Default triggers for external dependencies must be yes, no, or auto
for gxc_cfg_default in ${gxc_cfg_extdeps}; do
  tmp_default=`eval echo \$\{gxc_${gxc_cfg_default}_enable_def\}`
  AC_MSG_CHECKING([default detection behavior for ${gxc_cfg_default}])
  AC_MSG_RESULT([${tmp_default}])
  if test "${tmp_default}" != "" -a \
          "${tmp_default}" != "auto" -a \
          "${tmp_default}" != "no" -a \
          "${tmp_default}" != "yes"; then
    AC_MSG_ERROR([invalid default value: gxc_${gxc_cfg_default}_enable_def = ${tmp_default}])
  fi
done

# Clean-up
unset tmp_default

                    # ------------------------------------ #

#
# Optional features
#

# Conditional compilation (alphabetical order)
AC_ARG_ENABLE([debug],
  [AS_HELP_STRING([--enable-debug],
    [Enable verbose debugging messages (default: ${gxc_debug_enable_def})])],
  [gxc_debug_enable="${enableval}"; gxc_debug_type="yon"],
  [gxc_debug_enable="${gxc_debug_enable_def}"; gxc_debug_type="def"])
AC_SUBST(enable_debug)
AC_ARG_ENABLE([multiconfig],
  [AS_HELP_STRING([--enable-multiconfig],
    [Change library name depending on single-/double-precision and MPI support, a la FFTW, for hosts where multiple build flavors are installed concurrently on the system (default: ${gxc_multiconfig_enable_def})])],
  [gxc_multiconfig_enable="${enableval}"; gxc_multiconfig_type="yon"],
  [gxc_multiconfig_enable="${gxc_multiconfig_enable_def}"; gxc_multiconfig_type="def"])
AC_SUBST(enable_multiconfig)
AC_ARG_ENABLE([old-cray],
  [AS_HELP_STRING([--enable-old-cray],
    [Enable legacy Cray directives in the source code (default: ${gxc_old_cray_enable_def})])],
  [gxc_old_cray_enable="${enableval}"; gxc_old_cray_type="yon"],
  [gxc_old_cray_enable="${gxc_old_cray_enable_def}"; gxc_old_cray_type="def"])
AC_SUBST(enable_old_cray)
AC_ARG_ENABLE([single-precision],
  [AS_HELP_STRING([--enable-single-precision],
    [Enable single-precision grids (default: ${gxc_single_enable_def})])],
  [gxc_single_enable="${enableval}"; gxc_single_type="yon"],
  [gxc_single_enable="${gxc_single_enable_def}"; gxc_single_type="def"])
AC_SUBST(enable_single_precision)

                    # ------------------------------------ #

#
# External dependencies
#

# LibXC (optional)
AC_ARG_WITH([libxc],
  [AS_HELP_STRING([--with-libxc],
    [Install prefix of the LibXC libraries (e.g. '/usr/local'), or comma-separated compilation directives for includes and libraries (e.g. '-I/usr/include,-L/usr/lib64 -llibxc').])],
  [ if test "${withval}" = "no" -o "${withval}" = "yes"; then
      gxc_libxc_enable="${withval}"
      gxc_libxc_type="yon"
      if test "${withval}" = "yes"; then
        gxc_libxc_incs="${gxc_libxc_incs_def}"
        gxc_libxc_libs="${gxc_libxc_libs_def}"
      fi
    else
      gxc_libxc_enable="yes"
      tmp_comma=`echo "${withval}" | grep ','`
      if test "${tmp_comma}" = ""; then
        gxc_libxc_type="pfx"
        gxc_libxc_incs="-I${withval}/include"
        gxc_libxc_libs="-L${withval}/lib ${gxc_libxc_libs_def}"
      else
        gxc_libxc_type="csv"
        gxc_libxc_incs="`echo ${withval} | cut -d',' -f1`"
        gxc_libxc_libs="`echo ${withval} | cut -d',' -f2`"
      fi
    fi],
  [gxc_libxc_enable="${gxc_libxc_enable_def}"; gxc_libxc_type="def";
    gxc_libxc_incs="${gxc_libxc_incs_def}"; gxc_libxc_libs="${gxc_libxc_libs_def}"])
AC_SUBST(with_libxc)

# MPI (optional)
AC_ARG_WITH([mpi],
  [AS_HELP_STRING([--with-mpi],
    [Install prefix of MPI (e.g. /usr/local). The default behaviour is to detect whether the specified compilers properly support MPI and to fall back to serial mode if not. You may use --with-mpi without argument to force MPI detection, in which case detection failures will result in errors, and --without-mpi to disable MPI support completely.])],
  [ if test "${withval}" = "no" -o "${withval}" = "yes"; then
      gxc_mpi_enable="${withval}"
      gxc_mpi_type="yon"
    else
      gxc_mpi_enable="yes"
      tmp_comma=`echo "${withval}" | grep ','`
      if test "${tmp_comma}" = ""; then
        gxc_mpi_type="pfx"
        gxc_mpi_incs="-I${withval}/include"
        gxc_mpi_libs="-L${withval}/lib ${gxc_mpi_libs_def}"
      else
        gxc_mpi_type="csv"
        gxc_mpi_incs="`echo ${withval} | cut -d',' -f1`"
        gxc_mpi_libs="`echo ${withval} | cut -d',' -f2`"
      fi
    fi],
  [gxc_mpi_enable="${gxc_mpi_enable_def}"; gxc_mpi_type="def"])
AC_SUBST(with_mpi)

                    # ------------------------------------ #

#
# Check option consistency
#

# All --enable-* options must be yes or no
for gxc_cfg_option in ${gxc_cfg_switches}; do
  tmp_option=`eval echo \$\{enable_${gxc_cfg_option}\}`
  if test "${tmp_option}" != "" -a \
          "${tmp_option}" != "no" -a \
          "${tmp_option}" != "yes"; then
    AC_MSG_ERROR([--enable-${gxc_cfg_option} must be "yes" or "no"])
  fi
done

# Improve over default values when possible
if test \( "${gxc_libxc_type}" = "def" -o "${gxc_libxc_type}" = "yon" \) -a \
           "${gxc_libxc_enable}" != "no"; then
  AC_MSG_CHECKING([whether LibXC was installed through EasyBuild])
  if test -n "${EBROOTLIBXC}"; then
    gxc_libxc_enable="yes"
    gxc_libxc_type="ebm"
    gxc_libxc_incs="-I${EBROOTLIBXC}/include"
    gxc_libxc_libs="-L${EBROOTLIBXC}/lib ${gxc_libxc_libs_def}"
    AC_MSG_RESULT([yes])
  else
    AC_MSG_RESULT([no])
  fi
fi

# Adjust floating-point precision depending on specified options
if test "${gxc_single_enable}" = "yes"; then
  gxc_precision="single"
fi

# Clean-up
unset tmp_option

                    # ------------------------------------ #

#
# Substitute build-system variables
#

# Defaults
AC_SUBST(gxc_debug_enable_def)
AC_SUBST(gxc_multiconfig_enable_def)
AC_SUBST(gxc_old_cray_enable_def)
AC_SUBST(gxc_single_enable_def)
AC_SUBST(gxc_libxc_enable_def)
AC_SUBST(gxc_libxc_incs_def)
AC_SUBST(gxc_libxc_libs_def)
AC_SUBST(gxc_mpi_enable_def)
AC_SUBST(gxc_mpi_incs_def)
AC_SUBST(gxc_mpi_libs_def)

# Triggers
AC_SUBST(gxc_debug_enable)
AC_SUBST(gxc_multiconfig_enable)
AC_SUBST(gxc_old_cray_enable)
AC_SUBST(gxc_single_enable)

# Initialization types
AC_SUBST(gxc_debug_type)
AC_SUBST(gxc_multiconfig_type)
AC_SUBST(gxc_old_cray_type)
AC_SUBST(gxc_single_type)
AC_SUBST(gxc_libxc_type)
AC_SUBST(gxc_mpi_type)

# Build flags for external dependencies
AC_SUBST(gxc_libxc_incs)
AC_SUBST(gxc_libxc_libs)
AC_SUBST(gxc_mpi_incs)
AC_SUBST(gxc_mpi_libs)

# Internal variables
AC_SUBST(ARCH_SUFFIX)
AC_SUBST(PREC_SUFFIX)
AC_SUBST(gxc_precision)

                    # ------------------------------------ #

#
# Architecture setup
#

# Init MPI parameters
GXC_MPI_INIT

                    # ------------------------------------ #

#
# C language support
#

# Init C compiler and preprocessor
AC_PROG_CC
AC_PROG_CPP

# C compiler peculiarities (for Libtool)
AM_PROG_CC_C_O

                    # ------------------------------------ #

#
# Fortran language support
#

# Look for the Fortran compiler
if test "${FC}" != "" -a ! -x "${FC}"; then
  gxc_fc_probe=`echo "${FC}" | sed -e 's/ .*//'`
  if test ! -x "${gxc_fc_probe}"; then
    AC_PATH_PROG([gxc_fc_path],[${gxc_fc_probe}])
    if test "${gxc_fc_path}" = ""; then
      AC_MSG_ERROR([could not run Fortran compiler "${FC}"])
    fi
  fi
fi
AC_PROG_FC

# Fail if no Fortran compiler is available
if test "${FC}" = ""; then
  AC_MSG_ERROR([no Fortran compiler available])
fi

# Get module file case
GXC_FC_MOD_CASE

# FIXME: legacy Fortran files should be upgraded to Fortran 2003
AC_ARG_VAR([F77], [Fortran 77 compiler])
if test "${F77}" = ""; then
  F77="${FC}"
fi

                    # ------------------------------------ #

#
# Libtool configuration
#

# Init Libtool (must be done once compilers are fully set)
LT_INIT
LT_PREREQ([2.4.2])
LT_LANG([Fortran])
LTOBJEXT="lo"
AC_SUBST(LTOBJEXT)

                    # ------------------------------------ #

#
# Multicore architecture support
#

# Look for MPI
if test "${gxc_mpi_enable}" != "no"; then
  GXC_MPI_DETECT
  if test "${gxc_mpi_ok}" = "yes"; then
    AC_DEFINE([HAVE_MPI], 1, [Define to 1 to enable MPI support.])
    gxc_mpi_enable="yes"
  else
    if test "${gxc_mpi_enable}" = "auto"; then
      AC_MSG_NOTICE([disabling MPI support])
      gxc_mpi_enable="no"
    else
      AC_MSG_FAILURE([MPI support is broken - please check your configuration])
    fi
  fi
fi

# From now on, the MPI trigger must be yes or no
if test "${gxc_mpi_enable}" != "no" -a "${gxc_mpi_enable}" != "yes"; then
  AC_MSG_ERROR([unexpected MPI trigger value: '${gxc_mpi_enable}'])
fi

                    # ------------------------------------ #

#
# External dependencies (the order matters a lot)
#

# Check for mathematical functions
AC_CHECK_LIB(m, sqrt, [], [
  AC_MSG_WARN([missing math library (sqrt not found)])])

# LibXC
if test "${gxc_libxc_enable}" != "no"; then
  AC_MSG_CHECKING([how LibXC parameters have been set])
  AC_MSG_RESULT([${gxc_libxc_type}])
  AC_MSG_CHECKING([for LibXC include flags])
  AC_MSG_RESULT([${gxc_libxc_incs}])
  AC_MSG_CHECKING([for LibXC library flags])
  AC_MSG_RESULT([${gxc_libxc_libs}])
  GXC_LIBXC_DETECT
  if test "${gxc_libxc_ok}" = "yes"; then
    gxc_libxc_enable="yes"
    CPPFLAGS="${CPPFLAGS} ${gxc_libxc_incs}"
    FCFLAGS="${FCFLAGS} ${gxc_libxc_incs}"
    LIBS="${gxc_libxc_libs} ${LIBS}"
  else
    gxc_libxc_enable="no"
    if test "${gxc_libxc_type}" != "def" -a "${gxc_libxc_type}" != "ebm"; then
      AC_MSG_FAILURE([LibXC is missing or incomplete])
    fi
  fi
fi

                    # ------------------------------------ #

#
# Adjust build parameters according to configure options
#

# Conditional compilation (alphabetical order)
if test "${gxc_debug_enable}" = "yes"; then
  AC_DEFINE([DEBUG_XC], 1,
    [Define to 1 if you want to enable debugging.])
fi
if test "${gxc_old_cray_enable}" = "yes"; then
  AC_DEFINE([OLD_CRAY], 1,
    [Define to 1 if you have an oldish Cray architecture.])
fi
if test "${gxc_single_enable}" = "yes"; then
  AC_DEFINE([GRID_SP], 1,
    [Define to 1 if you want to use single-precision grids.])
else
  AC_DEFINE([GRID_DP], 1,
    [Define to 1 if you want to use double-precision grids.])
fi
if test "${gxc_libxc_enable}" = "yes"; then
  AC_DEFINE([HAVE_LIBXC], 1,
    [Define to 1 if you want to use LibXC functionals.])
fi
if test "${gxc_mpi_enable}" = "yes"; then
  AC_DEFINE([HAVE_MPI], 1,
    [Define to 1 if you want to use MPI.])
fi

# Inform Automake on MPI status
AM_CONDITIONAL([GRIDXC_USES_LIBXC], [test "${gxc_libxc_enable}" = "yes"])
AM_CONDITIONAL([GRIDXC_USES_MPI], [test "${gxc_mpi_enable}" = "yes"])
AM_CONDITIONAL([GRIDXC_USES_MULTICONFIG], [test "${gxc_multiconfig_enable}" = "yes"])

# Set architecture suffix for library name
if test "${gxc_multiconfig_enable}" = "yes" -a "${gxc_mpi_enable}" = "yes"; then
  ARCH_SUFFIX="_mpi"
fi

# Set precision suffix for library name
if test "${gxc_multiconfig_enable}" = "yes"; then
  case "${gxc_precision}" in
    single)
      PREC_SUFFIX="_sp"
      ;;
    double)
      PREC_SUFFIX="_dp"
      ;;
    *)
      AC_MSG_ERROR([invalid floating-point precision: ${gxc_precision}])
      ;;
  esac
fi

                    # ------------------------------------ #

#
# Propagate configuration
#

# FIXME: copy Fortran flags for legacy source files
FFLAGS="${FCFLAGS}"

# Transmit relevant information to pkg-config
gxc_libxc_req_pc=""
if test "${gxc_libxc_enable}" = "yes"; then
  gxc_libxc_req_pc="libxc"
fi
AC_SUBST(gxc_libxc_req_pc)

# Transmit LibXC information to SIESTA
gxc_siesta_libxc="0"
if test "${gxc_libxc_enable}" = "yes"; then
  gxc_siesta_libxc="1"
fi
AC_SUBST(gxc_siesta_libxc)

# Transmit MPI information to SIESTA
gxc_siesta_mpi="0"
if test "${gxc_mpi_enable}" = "yes"; then
  gxc_siesta_mpi="1"
fi
AC_SUBST(gxc_siesta_mpi)

                    # ------------------------------------ #

# Write down YAML configuration now (allows full report with deferred errors)
AC_OUTPUT([config/data/libgridxc-config.yml])

# Report configuration
AC_MSG_NOTICE([])
AC_MSG_NOTICE([Final build parameters])
AC_MSG_NOTICE([----------------------])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([TSTAMP     = ${gxc_timestamp}])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([CPP        = ${CPP}])
AC_MSG_NOTICE([CPPFLAGS   = ${CPPFLAGS}])
AC_MSG_NOTICE([CC         = ${CC}])
AC_MSG_NOTICE([CFLAGS     = ${CFLAGS}])
AC_MSG_NOTICE([FC         = ${FC}])
AC_MSG_NOTICE([FCFLAGS    = ${FCFLAGS}])
AC_MSG_NOTICE([LDFLAGS    = ${LDFLAGS}])
AC_MSG_NOTICE([LIBS       = ${LIBS}])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([Debugging        : ${gxc_debug_enable} (init: ${gxc_debug_type})])
AC_MSG_NOTICE([Multiconfig      : ${gxc_multiconfig_enable} (init: ${gxc_multiconfig_type})])
AC_MSG_NOTICE([Oldish Cray      : ${gxc_old_cray_enable} (init: ${gxc_old_cray_type})])
AC_MSG_NOTICE([Single precision : ${gxc_single_enable} (init: ${gxc_single_type})])
AC_MSG_NOTICE([LibXC            : ${gxc_libxc_enable} (init: ${gxc_libxc_type})])
AC_MSG_NOTICE([MPI              : ${gxc_mpi_enable} (init: ${gxc_mpi_type})])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([Library name     : libgridxc${PREC_SUFFIX}${ARCH_SUFFIX}.a])
AC_MSG_NOTICE([])

                    # ------------------------------------ #

#
# Output configuration
#

AC_OUTPUT([
  config/data/gridxc.mk
  config/data/libgridxc.pc
  config/data/libxc.mk
  Makefile
  src/Makefile
  docs/Makefile
  Testers/Makefile
])
